﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine
{
    public State state;

    public StateMachine(State state = null)
    {
        this.state = state;
    }

    public virtual void Update()
    {
        state?.Tick();
    }

    public virtual void SetState(State state)
    {
        this.state?.OnExit();
        this.state = state;
        this.state?.OnEnter();
    }
    
    /*public virtual void RemoveState(State state)
    {
        Debug.Log("Remove state base");
    }*/
}
