﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface State
{ 
   StateMachine stateMachine { get; set; }

   void OnEnter();

   void Tick();

   void OnExit();

}
