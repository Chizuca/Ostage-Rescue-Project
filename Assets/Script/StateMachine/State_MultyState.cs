﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class State_MultyState : State
{
    public StateMachine stateMachine { get; set; }
    
    public List<StateMachine> statesMachines = new List<StateMachine>();
    
    public State_MultyState(List<State> states = null) : base()
    {
        foreach (var state in states)
        {
            AddState(state);
        }
    }

    public void OnEnter()
    {
       
    }

    public void Tick()
    {
        for (int i = 0; i < statesMachines.Count; i++)
        {
            statesMachines[i]?.Update();
        }
    }

    public void OnExit()
    {
      
    }

    public State FindState(Type t)
    {
        foreach (var stateMach in statesMachines)
        {
            if (stateMach.state != null && (stateMach.state.GetType() == t || stateMach.state.GetType().IsSubclassOf(t)))
            {
                //Debug.Log(stateMach.state);
                return stateMach.state;
            }
        }

        return null;
    }

    public void AddState(State state)
    {
        if (state != null)
        {
            state.OnEnter();
            statesMachines.Add(new StateMachine(state));
            statesMachines[statesMachines.Count - 1].state.stateMachine = statesMachines[statesMachines.Count - 1];
        }
    }

    public void RemoveState(State state)
    {
        for (int i = 0; i < statesMachines.Count; i++)
        {
            if (statesMachines[i].state == state)
            {
                statesMachines[i].state?.OnExit();
                statesMachines.RemoveAt(i);
            }
        }
    }
    
    public void RemoveState(List<State> states)
    {
        for (int i = 0; i < statesMachines.Count; i++)
        {
            for (int j = 0; j < states.Count; j++)
            {
                if (statesMachines[i].state == states[j])
                {
                    statesMachines[i].state?.OnExit();
                    statesMachines.RemoveAt(i);
                }
            }
        }
    }
    
    public void RemoveState(Type t)
    {
        for (int i = 0; i < statesMachines.Count; i++)
        {
            if (statesMachines[i].state?.GetType() == t)
            {
                statesMachines[i].state?.OnExit();
                statesMachines.RemoveAt(i);
            }
        }
    }
    
    public void RemoveState(List<Type> types)
    {
        List<int> indexs = new List<int>();
        
        for (int i = 0; i < statesMachines.Count; i++)
        {
            for (int j = 0; j < types.Count; j++)
            {
                if (statesMachines[i].state?.GetType() == types[j])
                {
                    indexs.Add(i);
                }
            }
        }

        foreach (var index in indexs)
        {
            statesMachines[index].state?.OnExit();
            statesMachines.RemoveAt(index);
        }
    }
}
