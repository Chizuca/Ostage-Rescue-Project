﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class State_Patrolling : State_Enemys
{
    public State nextState;
    
    public float patrolTimer;

    public State_Patrolling(Enemys enemys, StateMachine stateMachine, State nextState) : base(enemys)
    {
        this.nextState = nextState;
        this.stateMachine = stateMachine;
    }

    public override void OnEnter()
    {
        //Debug.Log(enemys.name + "Enter State : State_Patrolling");
    }

    public override void Tick()
    {
        //Movements patrolling

        if (!enemys.NavMeshAgent.hasPath)
        {
            patrolTimer -= Time.deltaTime;

            patrolTimer = Mathf.Clamp(patrolTimer, 0, 10f);

            if (patrolTimer <= 0)
            {
                SetNewPatrolPos();
                patrolTimer = enemys.timeBetweenPatrols;
            }
        }
    }

    public override void OnExit()
    {
      
    }

    public void SetNewPatrolPos() //create circle with random size get a random position on this circle and set destination to pos + position on circle
    {
        float posOnCircle = Random.Range(0, 2 * Mathf.PI);
                    
        float circleRange = Random.Range(1, 10);

        float x = Mathf.Cos(posOnCircle) * circleRange;
        float z = Mathf.Sin(posOnCircle) * circleRange;
        
        enemys.NavMeshAgent.SetDestination(Vector3.Lerp(enemys.transform.position + new Vector3(x, 0, z), Gears.gears.player.transform.position, 0.1f));
    }

    public override void OnSeePlayer()
    {
        //Debug.Log("OnSeePlayer_State_Patroling");
        nextState.stateMachine = stateMachine;
        stateMachine.SetState(nextState);
    }
}
