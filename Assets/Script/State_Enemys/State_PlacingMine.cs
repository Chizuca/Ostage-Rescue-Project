﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class State_PlacingMine : State_Enemys
{
    public MineScript currentMine;

    public float timeBetweenMines;
    public float _timeBetweenMines;
    public float dmg;
    
    public State_PlacingMine(Enemys enemys, StateMachine stateMachine, float timeBetweenMines, float dmg) : base(enemys)
    {
        this.stateMachine = stateMachine;
        this.timeBetweenMines = timeBetweenMines;
        this.dmg = dmg;
    }

    public override void OnEnter()
    {
        //Debug.Log(enemys.name + "Enter State : State_PlacingMine");
    }

    public override void Tick()
    {
        _timeBetweenMines -= Time.deltaTime;
        
        if (_timeBetweenMines <= 0 && !currentMine)
        {
            RaycastHit hit;
            
            if (Physics.Raycast(enemys.transform.position, Vector3.down, out hit, 100f, Gears.gears.walkableLayer))
            {
                //Place mine on the ground using ray
                currentMine = MonoBehaviour.Instantiate(Gears.gears.mine, hit.point, enemys.transform.rotation).GetComponent<MineScript>();
                currentMine.dmg = dmg;
            }

            _timeBetweenMines = timeBetweenMines;
        }

        _timeBetweenMines = Mathf.Clamp(_timeBetweenMines, 0, 10);
    }

    public override void OnExit()
    {
      
    }

    public override void OnSeePlayer()
    {
        
    }
}