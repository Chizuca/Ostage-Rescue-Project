﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class State_GoTo_Attack : State_Enemys
{
    public State nextState;
    
    public Action attack;
    public Action notInRangeAction;
    public Action inRangeAction;
    
    public Action onEnter;
    public Action onExit;

    public Action onStopSeePlayer;

    public bool inRange;
    
    public Quaternion rot;
    public Vector3 targetRot;

    public State_GoTo_Attack(Enemys enemys, StateMachine stateMachine, State nextState, Action attack, Action notInRangeAction, Action onStopSeePlayer = null, 
        Action inRangeAction = null, Action onEnter = null, Action onExit = null) : base(enemys)
    {
        this.stateMachine = stateMachine;
        
        this.nextState = nextState;
        
        this.attack = attack;
        this.notInRangeAction = notInRangeAction;
        this.inRangeAction = inRangeAction;

        this.onEnter = onEnter;
        this.onExit = onExit;
        
        this.onStopSeePlayer = onStopSeePlayer;
    }

    public override void OnEnter()
    {
        onEnter?.Invoke();
    }

    public override void Tick()
    {
        if (Vector3.Distance(Gears.gears.player.transform.position, enemys.transform.position) < enemys.attackRange)
        {
            if (!inRange)
            {
                //Go to player
                enemys.NavMeshAgent.SetDestination(enemys.transform.position);
                inRange = true;
            }
            
            if (enemys.attackTimer <= 0)
            {
                //attack
                attack?.Invoke();
                enemys.attackTimer = 1 / enemys.attackSpeed;
            }
            
            //Turn/watch player
            targetRot = new Vector3(Gears.gears.player.transform.position.x - enemys.transform.position.x, enemys.transform.position.y, 
                Gears.gears.player.transform.position.z - enemys.transform.position.z);
            
            rot = new Quaternion(rot.x, Quaternion.LookRotation(targetRot).y, rot.z, Quaternion.LookRotation(targetRot).w);
            enemys.transform.rotation = Quaternion.Slerp(enemys.transform.rotation, rot, 10 * Time.deltaTime);
            
            inRangeAction?.Invoke();
        }
        else
        {
            if (inRange)
            {
                //Debug.Log("Not In Range");
                inRange = false;
            }
            
            //Movements not In Range
            notInRangeAction?.Invoke();
        }
    }

    public override void OnExit()
    {
        onExit?.Invoke();
    }
    
    public override void OnSeePlayer()
    {
        
    }

    public override void StopSeePlayer()
    {
        onStopSeePlayer?.Invoke();

        if (nextState != null)
        {
            nextState.stateMachine = stateMachine;
            stateMachine.SetState(nextState);
        }
    }
}