﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class State_Speed_Nav : State
{
    public StateMachine stateMachine { get; set; }
    
    public NavMeshAgent navMeshAgent;
    public float baseMovementSpeed;
    public float increasedMovementSpeed;
    public float timerSpeed;

    public Action onExit;
    
    public State_Speed_Nav(StateMachine stateMachine, NavMeshAgent navMeshAgent, float baseMovementSpeed, float increasedMovementSpeed, float timerSpeed, Action onExit = null) : base()
    {
        this.stateMachine = stateMachine;
        this.navMeshAgent = navMeshAgent;
        this.baseMovementSpeed = baseMovementSpeed;
        this.increasedMovementSpeed = increasedMovementSpeed;
        this.timerSpeed = timerSpeed;

        this.onExit = onExit;
    }

    public void OnEnter()
    {
        //Debug.Log("Buff Speed");
        navMeshAgent.speed = increasedMovementSpeed;
    }

    public void Tick()
    {
        timerSpeed -= Time.deltaTime;

        if (timerSpeed <= 0 || !navMeshAgent.hasPath)
        {
            stateMachine.SetState(null);
        }
    }

    public void OnExit()
    {
        //Debug.Log("OnExit buff speed");
        onExit?.Invoke();
        navMeshAgent.speed = baseMovementSpeed;
    }
}
