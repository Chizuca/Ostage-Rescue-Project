﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class State_RangeAttack_Phase02 : State_Enemys
{
    public Vector3 pos;

    public Vector3 dest;

    public State_Enemys nextState;
    
    public State_RangeAttack_Phase02(Enemys enemys, Vector3 pos, State_Enemys nextState) : base(enemys)
    {
        this.pos = pos;

        this.nextState = nextState;
    }

    public override void OnEnter()
    {
        //Debug.Log("Enter Go to pos and lock");
    }

    public override void Tick()
    {
        if (!enemys.NavMeshAgent.hasPath) //go to tower point
        {
            enemys.NavMeshAgent.SetDestination(pos);
            dest = enemys.NavMeshAgent.destination;
        }
        
        if (Vector3.Distance(enemys.transform.position, dest) < 1.5f) //if tower point reached stay still
        {
            enemys.NavMeshAgent.isStopped = true;
            enemys.stateMachine.SetState(nextState);
        }
    }

    public override void OnExit()
    {
        
    }

    public override void OnSeePlayer()
    {
        
    }

    public override void StopSeePlayer()
    {
        
    }
}
