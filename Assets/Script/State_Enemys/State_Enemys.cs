﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class State_Enemys : State
{
    public StateMachine stateMachine { get; set; }
    
    public Enemys enemys;

    public State_Enemys(Enemys enemys) : base()
    {
        this.enemys = enemys;
    }

    public virtual void OnEnter()
    {
      
    }

    public virtual void Tick()
    {
        
    }

    public virtual void OnExit()
    {
      
    }

    public virtual void OnSeePlayer()
    {
        
    }

    public virtual void StopSeePlayer()
    {
        
    }
}
