﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class HostageFollow : MonoBehaviour
{
    public NavMeshAgent hostage;
    public Transform player;

    public bool free = false;

    
    
    public float radius = 10f;
    public float radiusWall = 20f;

    private Vector3 direction;

    public Renderer rend;
    public float hp = 1;

    private Vector3 destination;
    private Vector3 destination2;
    private Vector3 destination3;
    private Vector3 destination4;

    public GameObject gameOver;

    public bool safe;
    public bool posNotSafe;

    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (free && safe) //L'otage suit le joueur si il est en phase 2 et sans ennemis autour
        {
            hostage.SetDestination(player.position);
        }
        
        Collider[] collidersWall = Physics.OverlapSphere(transform.position, radiusWall); //detect wall

        foreach (Collider coll in collidersWall)
        {
            if (coll.tag == "Wall")
            {
                rend = coll.gameObject.GetComponent<Renderer>();
                /*Debug.Log(rend.bounds.extents.x);
                Debug.Log(rend.bounds.extents.y);
                Debug.Log(coll.transform.position.x);
                Debug.Log(coll.transform.position.y);*/
                
                destination = new Vector3(coll.transform.position.x, 0,coll.transform.position.z + rend.bounds.extents.z) ;
                destination2 = new Vector3(coll.transform.position.x, 0,coll.transform.position.z - rend.bounds.extents.z) ;
                destination3 = new Vector3(coll.transform.position.x + rend.bounds.extents.x , 0,  coll.transform.position.z);
                destination4 = new Vector3(coll.transform.position.x - rend.bounds.extents.x ,0, coll.transform.position.z); //analyse wall and detect position of faces

                Debug.Log(destination + "destination"); 
            }
        }

        //int layerMask = 1 << 9;
        Collider[] colliders = Physics.OverlapSphere(transform.position, radius); //detect enemy

        foreach (Collider col in colliders)
        {
            direction = col.transform.position - transform.position;
           
            if (col.tag == "Enemy")
            {

                if (free == true) //launch hide function for hostage if ennemy is near
                {
                    Hide();
                }
                
                
                
                
            }
            
        }

    }

    public void Hide()
    {
        RaycastHit hits;
        
        if (Physics.Raycast(transform.position, (direction), out hits)) //detect if there is something beetween enemy and hostage
        {
            if (hits.collider.gameObject.name.Contains("Enemy"))
            {
                Debug.Log("pas cacher");


                safe = false;
                    
                    
                StartCoroutine(GotoPos(destination)); //if hostage is not hide launch his movement

                if ((transform.position - destination).sqrMagnitude < 12 && safe == false) //if the position is not safe, change bool to go to the other position
                {
                    posNotSafe = true;
                    Debug.Log("pos1notsafe");
                }
                
                if ((transform.position - destination2).sqrMagnitude < 12 && safe == false)  //if the position is not safe, change bool to go to the other position
                {
                    posNotSafe = false;
                    Debug.Log("pos2notsafe");
                }
            }
            else //hostage is hidden

            {
                Debug.Log("cacher");
            }
            
            

        }
    }
    
    IEnumerator GotoPos(Vector3 pos)
    {
        if (posNotSafe == false) //hostage go to a position
        {
            hostage.SetDestination(destination);
        }

        
        yield return new WaitUntil(() =>  (transform.position - pos).sqrMagnitude < 12); //when he's at the postion and he's not safe
        Debug.Log("est a la postion 1");
        
        ;

        if (posNotSafe == true) //move to other position
        {
            Debug.Log("des2");
            hostage.SetDestination(destination2);
            
        }
                
                
            
            
        


    }

    public void TakeDamage(float damage) //damage for the hostage
    {
        hp -= damage;
        if (hp <= 0)
        {
            Destroy(gameObject);
            gameOver.SetActive(true);
            Time.timeScale = 0;
        }
    }
    
    private void OnDrawGizmosSelected() //see the hitbox and detection range
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, radius);
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, radiusWall);
        
        // Gizmos.DrawLine(transform.position, direction);
    }
}