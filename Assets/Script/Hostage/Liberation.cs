﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Liberation : MonoBehaviour
{
    public float libeTimer = 3;
    public float pressButton = 0;

    public bool playerNear;

    public Transform textPos;
    public RectTransform text;
    public RectTransform textRescue;

    // Start is called before the first frame update
    void Start()
    {
        text.transform.SetParent(Gears.gears.mainCanvas.transform); 
        
        textRescue.transform.SetParent(Gears.gears.mainCanvas.transform);
        textRescue.GetComponent<RectTransform>().position = Camera.main.WorldToScreenPoint(textPos.position);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.E) && playerNear) //release the hostage when player is near and press E
        {
            pressButton += Time.deltaTime;

            if (pressButton >= libeTimer) // you need to press for X second
            {
                transform.parent.Translate(Vector3.left * Time.deltaTime);  
                transform.parent.gameObject.GetComponent<HostageFollow>().free = enabled; //change pattern of hostage with other script
            }
        }

        if (playerNear)
        {
            //display text "press e to free"
            if (!Gears.gears.hostageFollow.free)
            {
                text.GetComponent<RectTransform>().position = Camera.main.WorldToScreenPoint(textPos.position);
            }
        }
        else
        {
            //display text "Rescue"
            if (!Gears.gears.hostageFollow.free)
            {
                textRescue.GetComponent<RectTransform>().position = Camera.main.WorldToScreenPoint(textPos.position);
            }
        }
    }

     void OnTriggerEnter(Collider other)
    {
        
        if (other.tag == "Player") 
        {
            //display text "press e to free"
            if (!Gears.gears.hostageFollow.free)
            {
                text.GetComponent<RectTransform>().position = Camera.main.WorldToScreenPoint(textPos.position);
                text.gameObject.SetActive(true);
            }
            textRescue.gameObject.SetActive(false); //hide text for player "rescue"
            playerNear = true; 
            transform.parent.gameObject.GetComponent<HostageFollow>().safe = enabled; //activate safe bool for other script
        }
    }
    
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {          
            text.gameObject.SetActive(false);

            //hide text "press e to free"
            if (!Gears.gears.hostageFollow.free)
            {
                textRescue.gameObject.SetActive(true);
            }
            
            playerNear = false; //hostage is not safe if the player is not near. So he change to the other script
        }
    }

  
}
