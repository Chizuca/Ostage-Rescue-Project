﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VictoryZone : MonoBehaviour
{
    void Start()
    {
        
    }
    
    void Update()
    {
        
    }
    
    public void OnTriggerEnter(Collider coll)
    {
        HostageFollow hostageFollow = coll.GetComponent<HostageFollow>();

        //if collide with the hostage win
        if (hostageFollow)
        {
            Time.timeScale = 0;
            Gears.gears.victoryPanel.SetActive(true);
            //Debug.Log("Victory");
        }
    }
}
