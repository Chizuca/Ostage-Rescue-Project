﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Enemys_Range : Enemys
{
    [Header("Range Settings")]

    public GameObject bullet;

    public Transform firePoint;

    public LineRenderer sniperLine;

    public float bulletSpeed;
    public float fireSpread;

    public float timeBetweenBullets;
    public int nbrOfBulletsPerBurst;

    public override void Start()
    {
        base.Start();

        sniperLine.transform.position = firePoint.position;
        
        //Set behaviour
        State_Patrolling statePatrolling = new State_Patrolling(this, stateMachine, null);
        
        State_GoTo_Attack stateGoTo = new State_GoTo_Attack(this, stateMachine, statePatrolling, Attack, 
            () => NavMeshAgent.SetDestination(Gears.gears.player.transform.position));

        statePatrolling.nextState = stateGoTo;
        
        stateMachine.SetState(statePatrolling);
    }
    
    public override void Update()
    {
        base.Update();
    }
    
    public override void Attack()
    {
        //Debug.Log("Attack Range");
        if (seePlayer)
        {
            StartCoroutine(Shoot(timeBetweenBullets, nbrOfBulletsPerBurst));
        }
    }

    public IEnumerator Shoot(float timeBetweenBullets, int nbrOFBullets)
    {
        Bullet currentBullet = Instantiate(bullet, firePoint.position, bullet.transform.rotation).GetComponent<Bullet>();
        Vector3 dirP2 = Gears.gears.player.body.transform.position - firePoint.transform.position;
        
        currentBullet.dir = dirP2 + new Vector3(Random.Range(-fireSpread, fireSpread), Random.Range(-fireSpread, fireSpread));
        currentBullet.dmg = damage;
        currentBullet.speed = bulletSpeed;

        yield return new WaitForSeconds(timeBetweenBullets);

        if (nbrOFBullets > 1)
        {
            StartCoroutine(Shoot(timeBetweenBullets, nbrOFBullets - 1));
        }
    }

    public override void OnNextPhase()
    {
        movementSpeed = 10;
        SetMovementSpeed(movementSpeed);
        
        detectionRange = 30;
        attackRange = detectionRange;
        
        attackSpeed = 0.3f;
        damage += 1;

        nbrOfBulletsPerBurst = 1;
        fireSpread = 0.01f;
        bulletSpeed *= 4;

        horizontalAngle = 300f;

        Vector3 closestTowerPoint = Vector3.zero;
        float distance = Mathf.Infinity;

        for (int i = 0; i < Gears.gears.towerPoints.Length; i++)
        {
            if (Vector2.Distance(transform.position, Gears.gears.towerPoints[i].position) < distance)
            {
                distance = Vector2.Distance(transform.position, Gears.gears.towerPoints[i].position);
                closestTowerPoint = Gears.gears.towerPoints[i].position;
            }
        }

        if (closestTowerPoint == Vector3.zero)
        {
            Debug.Log("!Found no Tower point");
        }

        Action actions = null;
        actions += Attack;
        actions += () =>  sniperLine.SetPosition(0, firePoint.position);
        actions += () =>  sniperLine.SetPosition(1, firePoint.position);
            
        State_RangeAttack_Phase02 stateRangeAttackPhase02 = new State_RangeAttack_Phase02(this, closestTowerPoint, null);

        State_GoTo_Attack stateGoToAttack = new State_GoTo_Attack(this, stateMachine, 
            null, actions, null, null, () => LaserAim(Gears.gears.player.transform.position, 0.5f));

        stateRangeAttackPhase02.nextState = stateGoToAttack;
            
        stateMachine.SetState(stateRangeAttackPhase02);
    }

    public void LaserAim(Vector3 aimingPoint, float timeBeforeShoot) //set sniperLine positions
    {
        if (attackTimer <= timeBeforeShoot && seePlayer)
        {
            sniperLine.SetPosition(0, firePoint.position);
            sniperLine.SetPosition(1, aimingPoint);
        }
    }
}
