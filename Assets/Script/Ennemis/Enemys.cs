﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemys : MonoBehaviour
{
    [Header("Enemys Settings")]
    public NavMeshAgent NavMeshAgent;

    public StateMachine stateMachine = new StateMachine();

    public Transform eyePos;

    public Transform hpBarPos;
    public RectTransform hpBar;
    public Transform hpBarScaler;
    public float timerDisplayHp;

    public float maxHp;
    [NonSerialized] public float hp;
    
    public float damage;
    public float attackRange;
    public float attackSpeed;

    [NonSerialized] public Vector3 dirP;
    [NonSerialized] public float attackTimer;

    [Header("DetectionSettings")]
    public float movementSpeed;
    public float detectionRange;
    public float closeDetectionRange;
    public float horizontalAngle;
    public float timeBetweenPatrols;
    public float timeBeforeLoosingSight;
    
    public bool seePlayer;
    public bool displayHp;

    public virtual void Start()
    {
        NavMeshAgent = GetComponent<NavMeshAgent>();
        NavMeshAgent.speed = movementSpeed;

        hp = maxHp;
        
        hpBar.transform.SetParent(Gears.gears.mainCanvas.transform);
        hpBar.GetComponent<RectTransform>().rotation = new Quaternion(0,0,0, 0);

        //Set on Next phase if phase1 -> else OnNextPhase
        if (Gears.gears.hostageFollow.free)
        {
            OnNextPhase();
        }
        else
        {
            StartCoroutine(PrepareNextPhase());
        }
    }
    
    public virtual void Update()
    {
        stateMachine.Update();
        
        //direction player
        dirP = Gears.gears.player.camera.transform.position - eyePos.transform.position;

        //player detection
        if (Vector3.Distance(transform.position, Gears.gears.player.transform.position) < detectionRange && 
            Vector3.Angle(new Vector3(transform.forward.x, 0, transform.forward.z), new Vector3(dirP.normalized.x, 0, dirP.normalized.z)) < horizontalAngle)
        {
            Debug.DrawRay(eyePos.position, dirP, Color.green, 1f);
            
            RaycastHit hit;

            if (Physics.Raycast(eyePos.position, dirP.normalized, out hit, detectionRange) && (hit.collider.gameObject == Gears.gears.player.gameObject || 
                hit.collider.gameObject.transform.IsChildOf(Gears.gears.player.gameObject.transform)))
            {
                if (!seePlayer)
                {
                    //Debug.Log("See player");
                    OnSeePlayer();
                }
            }
            else if (seePlayer)
            {
                StopSeePlayer();
            }
        }
        else if (seePlayer && !(Vector3.Distance(transform.position, Gears.gears.player.transform.position) < closeDetectionRange))
        {
           StopSeePlayer();
        }
        
        if (Vector3.Distance(transform.position, Gears.gears.player.transform.position) < closeDetectionRange && !seePlayer)
        {
            OnSeePlayer();
        }

        attackTimer -= Time.deltaTime;

        attackTimer = Mathf.Clamp(attackTimer, 0, 10);

        if (displayHp)
        {
            hpBar.GetComponent<RectTransform>().position = Camera.main.WorldToScreenPoint(hpBarPos.position);
        }
    }

    public virtual void StopSeePlayer()
    {
        //Debug.Log("Stop see player");
        seePlayer = false;
        ((State_Enemys) stateMachine.state).StopSeePlayer();
    }

    public virtual void OnSeePlayer()
    {
        seePlayer = true;
        ((State_Enemys) stateMachine.state).OnSeePlayer();
    }

    public void TakeDamage(float Damage)
    {
        hp -= Damage;
        StartCoroutine(DisplayHp(timerDisplayHp));

        NavMeshAgent.SetDestination(Gears.gears.player.transform.position);

        if (hp <= 0)
        {
            Death();
        }
    }

    public void Death()
    {
        StopAllCoroutines();
        Destroy(hpBar.gameObject);
        Destroy(gameObject);
    }
    
    public void SetMovementSpeed(float speed)
    {
        //Debug.Log("Speed");
        NavMeshAgent.speed = speed;
    }

    public virtual void Attack()
    {
        Debug.Log("Base Attack");
    }

    public virtual void OnNextPhase()
    {
        Debug.Log("base OnNextPhase");
    }

    public IEnumerator PrepareNextPhase()
    {
        yield return new WaitUntil(() => Gears.gears.hostageFollow.free);
        
        OnNextPhase();
    }

    public IEnumerator DisplayHp(float timer) //display hpBar for timer and hide
    {
        displayHp = true;
        hpBar.GetComponent<RectTransform>().position = Camera.main.WorldToScreenPoint(hpBarPos.position);
        
        hpBar.gameObject.SetActive(true);
        hpBarScaler.localScale = new Vector3(hp / maxHp, hpBarScaler.localScale.y, hpBarScaler.localScale.z);

        yield return new WaitForSeconds(timer);

        displayHp = false;
        hpBar.gameObject.SetActive(false);
    }
}
