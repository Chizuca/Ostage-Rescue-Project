﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MineScript : MonoBehaviour
{
    public float dmg;
    public float distance;

    public Transform textPos;
    public RectTransform text;

    public ParticleSystem particleExplosion;

    void Start()
    {
        //setup text tooltip on UI
       text.transform.SetParent(Gears.gears.mainCanvas.transform);
       text.GetComponent<RectTransform>().rotation = new Quaternion(0,0,0, 0);
    }
    
    void Update()
    {
    //if player is close show tooltip text + allow destruction by pressing A
        if (Vector3.Distance(transform.position, Gears.gears.player.transform.position) < distance)
        {
            if (!text.gameObject.activeSelf)
            {
                text.gameObject.SetActive(true);
            }
            
            text.GetComponent<RectTransform>().position = Camera.main.WorldToScreenPoint(textPos.position);
            
            if (Input.GetButtonDown("A"))
            {
                Destroy(text.gameObject);
                Destroy(gameObject);
            }
        }else if (text.gameObject.activeSelf)
        {
            text.gameObject.SetActive(false);
        }
    }

    public void OnTriggerEnter(Collider coll)
    {
        //Debug.Log("Mine");
        
        PlayerSysteme playerSysteme = coll.gameObject.GetComponent<PlayerSysteme>();

        //damage player
        if (playerSysteme)
        {
            playerSysteme.TakeDamage((int) dmg);

            Destruction();
        }
        
        HostageFollow hostageFollow = coll.GetComponent<HostageFollow>();
        
        //damage hostage
        if (hostageFollow)
        {
            hostageFollow.TakeDamage((int) dmg);
            
            Destruction();
        }
    }

    public void Destruction()
    {
        if (particleExplosion)
        {
            particleExplosion.Play();
            Destroy(text.gameObject, particleExplosion.main.duration);
            Destroy(gameObject, particleExplosion.main.duration);
        }
        else
        {
            Destroy(text.gameObject);
            Destroy(gameObject);
        }
    }
}
