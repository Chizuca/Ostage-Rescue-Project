﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Enemys_Patrol : Enemys
{
    [Header("Patrol Settings")]
    public float escapeSpeedTimer;
    public float escapeSpeed;
    public float escapeRange;
    public float timeBetweenMines;
    
    public int nbrOfEnemysSummoned;

    public override void Start()
    {
        base.Start();

        attackRange = detectionRange;

        //Set behaviour
        State_Patrolling statePatrolling = new State_Patrolling(this, null, null);
        
        State_GoTo_Attack stateGoToAttack = new State_GoTo_Attack(this, stateMachine, statePatrolling, Attack, null);

        statePatrolling.nextState = stateGoToAttack;

        stateMachine.SetState(new State_MultyState(new List<State> {statePatrolling}));
    }
    
    public override void Update()
    {
        base.Update();
    }
    
    public override void Attack()
    {
        //Debug.Log("Attack Patrol");

        //Summon enemys
        for (int i = 0; i < nbrOfEnemysSummoned; i++)
        {
            bool enemy = Random.Range(0, 2) == 1;

            float posOnCircle = Random.Range(0, 2 * Mathf.PI);
                    
            float circleRange = Random.Range(1, 10);

            float x = Mathf.Cos(posOnCircle) * circleRange;
            float z = Mathf.Sin(posOnCircle) * circleRange;
            
            RaycastHit hit;

            if (Physics.Raycast(transform.position + new Vector3(x, 10, z), Vector3.down, out hit, 100f))
            {
                if (enemy)
                {
                    Instantiate(Gears.gears.enemy_Melee, new Vector3(transform.position.x + x, transform.position.y, transform.position.z + z), transform.rotation);
                }
                else
                {
                    Instantiate(Gears.gears.enemy_Range, new Vector3(transform.position.x + x, transform.position.y, transform.position.z + z), transform.rotation);
                }
            }
        }
    }

    public void RunAway()
    {
        ((State_MultyState) stateMachine.state).RemoveState(new List<Type>{typeof(State_Patrolling), typeof(State_GoTo_Attack), typeof(State_Speed_Nav)});
        
        State_Patrolling statePatrolling = new State_Patrolling(this, null, null);
        
        State_GoTo_Attack stateGoToAttack = new State_GoTo_Attack(this, stateMachine, statePatrolling, Attack, null);

        statePatrolling.nextState = stateGoToAttack;
        
        ((State_MultyState) stateMachine.state).AddState(
            new State_Speed_Nav(null, NavMeshAgent, movementSpeed, escapeSpeed, escapeSpeedTimer, 
                () => stateMachine.SetState(new State_MultyState(new List<State> {statePatrolling}))));

        Vector3 dirPlayer = Gears.gears.player.transform.position - transform.position;

        NavMeshAgent.SetDestination(transform.position - dirPlayer.normalized * escapeRange);
    }
    
    public override void StopSeePlayer()
    {
        seePlayer = false;
        ((State_Enemys) ((State_MultyState) stateMachine.state).FindState(typeof(State_Enemys)))?.StopSeePlayer();
    }

    public override void OnSeePlayer()
    {
        seePlayer = true;
        ((State_Enemys) ((State_MultyState) stateMachine.state).FindState(typeof(State_Enemys)))?.OnSeePlayer();
        ((State_Enemys) ((State_MultyState) stateMachine.state).FindState(typeof(State_Enemys)))?.Tick();

        if ((State_Enemys) ((State_MultyState) stateMachine.state).FindState(typeof(State_Enemys)) != null)
        {
            RunAway();
        }
    }

    public override void OnNextPhase()
    {
        ((State_MultyState) stateMachine.state).AddState(new State_PlacingMine(this, null, timeBetweenMines, damage));
    }
}
