﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemys_Melee : Enemys
{
    [Header("Melee Settings")]
    public float rushSpeed;

    public override void Start()
    {
        base.Start();

        //Set behaviour
        State_Patrolling statePatrolling = new State_Patrolling(this, stateMachine, null);
        
        State_GoTo_Attack stateGoTo = new State_GoTo_Attack(this, stateMachine, statePatrolling, Attack, 
            () => NavMeshAgent.SetDestination(Gears.gears.player.transform.position), () => SetMovementSpeed(movementSpeed), null, 
            () => SetMovementSpeed(rushSpeed));

        statePatrolling.nextState = stateGoTo;

        stateMachine.SetState(statePatrolling);
    }
    
    public override void Update()
    {
        base.Update();
    }

    public override void Attack()
    {
        //Debug.Log("Attack melee");
        Gears.gears.player.TakeDamage((int) damage);
    }

    public override void OnNextPhase()
    {
        rushSpeed += 8;
        movementSpeed += 5;
        damage += 1;
    }
}
