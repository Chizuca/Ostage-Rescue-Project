﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    /*public string tagEnabled;
    public LayerMask layerEnabled;*/

    public Vector3 dir;
    public float speed;
    public float dmg;
    
    void Start()
    {
        Destroy(gameObject, 3f);
        transform.rotation = Quaternion.LookRotation(dir, transform.up); //* new Quaternion(0, -90, 0, 0);
    }
    
    void Update()
    {
        transform.Translate(dir.normalized * speed * Time.deltaTime, Space.World);
    }

    public void OnTriggerEnter(Collider coll)
    {
        /*if (coll.gameObject.tag == tagEnabled || coll.gameObject.layer == layerEnabled)
        {
            
        }*/
        
        PlayerSysteme playerSysteme = coll.gameObject.GetComponent<PlayerSysteme>();

        if (playerSysteme)
        {
            //Debug.Log("Bullet");
            playerSysteme.TakeDamage((int) dmg);
            Destroy(gameObject);
        }

        HostageFollow hostageFollow = coll.GetComponent<HostageFollow>();
        
        if (hostageFollow)
        {
            hostageFollow.TakeDamage((int) dmg);
            Destroy(gameObject);
        }
    }
}
