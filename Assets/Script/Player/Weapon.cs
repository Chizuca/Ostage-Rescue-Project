﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Weapon : MonoBehaviour
{
    public float damage = 50f;
    public float range = 100f;
    public Camera fpsCam;
    
    public int bulletsMax = 10, bullets = -1;
    public TextMeshProUGUI bulletText;

    private void Start()
    {
        bullets = bulletsMax;
    }
    void Update()
    {
        //for display the number of bullet
        bulletText.text = bullets + "/" + bulletsMax;
        if (Input.GetButtonDown("Fire1") && bullets > 0)
        {
            //if player use "button one mouse" , he call this function Shoot
            Shoot();
        }

        if (bullets <= 0)
        {
            //for save the value 0 
            bullets = 0;
        }

        if (bullets >= bulletsMax)
        {
            //for save the bulletmax value
            bullets = bulletsMax;
        }
    }
    void Shoot()
    {
        //the shoot systeme use one bullet
        bullets--;
        RaycastHit hit;
        //and he shoot 
        if (Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit,range))
        {
            if (hit.transform.GetComponent<Enemys>())
            {
                hit.transform.GetComponent<Enemys>().TakeDamage(damage);
            }

        }
    }
}
