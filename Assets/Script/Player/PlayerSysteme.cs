﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerSysteme : MonoBehaviour
{
    public float health;
    public float maxHealth;
    public TextMeshProUGUI lifeText;
    
    public float speed = 10f;
    public float gravity = -9.81f;

    public float mouseSensivity = 100f;
    private float xRotation;
    public GameObject camera;
    public GameObject gameOverPanel;
    public Transform body;

    public GameObject[] weapons;
    public int actualWeapon;
    
    public void Start()
    {
        health = maxHealth;
        Cursor.lockState = CursorLockMode.Locked;
        weapons[1].SetActive(false);
    }
    void Update()
    {
        //display life of the player
        lifeText.text = health + "/" + maxHealth;
        //if the life of the player = 0 , the player are dead
        if (health <= 0)
        {
            Time.timeScale = 0f;
            Cursor.lockState = CursorLockMode.None;
            gameOverPanel.SetActive(true);
            health = 0;
        }
        //for health does'nt exceed maxhealth
        if (health >= maxHealth)
        {
            health = maxHealth;
        }

        //mouvement player value axe
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");
        //mouvement of player systeme 
        Vector3 move = transform.right * x + transform.forward * z;
        gameObject.GetComponent<CharacterController>().Move(move * speed * Time.deltaTime);
        //mouse axe valoue 
        float mouseX = Input.GetAxis("Mouse X") * mouseSensivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensivity * Time.deltaTime;
        //rotation of mouse systeme 
        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);
        //mouvement of mouse with camera systeme
        camera.transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
        gameObject.transform.Rotate(Vector3.up * mouseX);
    }

    public void TakeDamage(int dmg)
    {
        //if the fuction are call the player take damage
        health -= dmg;
    }
    //For the performances it is preferable to do with a principle of SetActive rather than destroy and instantiate, as if we plan to have several weapons for not overloaded.
    public void ChangeWeapon()
    {
        //systeme for switch weapon , depending on the current weapon
        if (actualWeapon == 0)
        {
            weapons[actualWeapon].SetActive(false);
            actualWeapon = 1;
        }
        else if (actualWeapon == 1)
        {
            weapons[actualWeapon].SetActive(false);
            actualWeapon = 0;
        }
        weapons[actualWeapon].SetActive(true);
    }
}
