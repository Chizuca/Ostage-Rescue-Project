﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gears : MonoBehaviour
{
    public static Gears gears;

    public PlayerSysteme player;
    public HostageFollow hostageFollow;
    
    public Canvas mainCanvas;

    public LayerMask playerLayer;
    public LayerMask walkableLayer;

    public Transform[] towerPoints;

    public bool paused;
    public GameObject pausePanel;
    public GameObject victoryPanel;

    [Header("Prefabs")] 
    public GameObject enemy_Melee;
    public GameObject enemy_Range;
    public GameObject enemy_Patrol;
    
    public GameObject mine;
    
    void Awake()
    {
        if (gears == null)
        {
            gears = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            gears.towerPoints = towerPoints;
            gears.player = player;
            gears.hostageFollow = hostageFollow;
            gears.mainCanvas = mainCanvas;
            gears.pausePanel = pausePanel;
            gears.victoryPanel = victoryPanel;
            Destroy(gameObject);
        }
    }
    
    void Start()
    {
        
    }
    
    void Update()
    {
        //pause
        if (Input.GetButtonDown("Cancel"))
        {
            Pause();
        }
    }

    public void Pause()
    {
        if (!paused)
        {
            Cursor.visible = true;
            pausePanel.SetActive(true);
            Time.timeScale = 0f;
        }
        else
        {
            Cursor.visible = false;
            Time.timeScale = 1f;
            pausePanel.SetActive(false);
        }
        paused = !paused;
    }

    public void Quit()
    {
        Application.Quit();
    }
}
