﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class ItemAmmo : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
         {
             //if collide the weapon add 10 bullets
             other.GetComponentInChildren<Weapon>().bullets += 10;
             Destroy(gameObject);
         }
}
