﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class ItemHeal : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        //if collide the player heal 10hp
        other.GetComponent<PlayerSysteme>().health += 10f;
        Destroy(gameObject);
    }

}
