﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    public GameObject [] itemsSpawn;
    public float timer , maxtimer;
    public GameObject itemSpawn;
    public void Update()
    {
        //gestion of timer, if items was spawn the timer stop
        if (itemSpawn == null)
        {
           timer += Time.deltaTime; 
        }
        //if timer > maxtimer and spawn havn't spawn object = item instanciante a random iteam in to itemsSpawn and reset timer
        if (timer >= maxtimer && itemSpawn == null)
        {
            itemSpawn = Instantiate(itemsSpawn[UnityEngine.Random.Range(0, itemsSpawn.Length)],gameObject.transform.position,gameObject.transform.rotation);
            timer = 0;
        }
    }
}

