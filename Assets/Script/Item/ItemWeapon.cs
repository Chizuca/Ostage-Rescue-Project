﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class ItemWeapon : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        //if collide the player use ChangeWeapon void
        other.GetComponent<PlayerSysteme>().ChangeWeapon();
        Destroy(gameObject);
    }
    
}
